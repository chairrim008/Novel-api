package com.novel.service;


import com.novel.domain.SysJobLog;

import java.util.List;

/**
 * 定时任务调度日志信息信息 服务层
 *
 * @author novel
 * @date 2020/3/2
 */
public interface SysJobLogService {
    /**
     * 获取quartz调度器日志的计划任务
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    List<SysJobLog> selectJobLogList(SysJobLog jobLog);

    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param id 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    SysJobLog selectJobLogById(Long id);

    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     */
    boolean addJobLog(SysJobLog jobLog);

    /**
     * 批量删除调度日志信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    boolean deleteJobLogByIds(Long[] ids);

    /**
     * 删除任务日志
     *
     * @param id 调度日志ID
     * @return 结果
     */
    boolean deleteJobLogById(Long id);

    /**
     * 清空任务日志
     *
     * @return 结果
     */
    boolean cleanJobLog();
}
