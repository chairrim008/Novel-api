package com.novel.kaptcha.cache.manager;

import com.novel.kaptcha.cache.KaptchaCache;
import com.novel.kaptcha.cache.impl.RedisKaptchaCache;
import com.novel.kaptcha.exception.KaptchaCacheException;

/**
 * 默认缓存管理器
 *
 * @author novel
 * @date 2019/12/2
 */
public class RedisKaptchaCacheManagerImpl extends AbstractKaptchaCacheManager {

    private final RedisKaptchaCache redisKaptchaCache;

    public RedisKaptchaCacheManagerImpl(RedisKaptchaCache redisKaptchaCache) {
        this.redisKaptchaCache = redisKaptchaCache;
    }

    @Override
    KaptchaCache createCache(String key) throws KaptchaCacheException {
        try {
            redisKaptchaCache.setPrefix(key);
            return redisKaptchaCache;
        } catch (Exception e) {
            throw new KaptchaCacheException();
        }
    }
}
