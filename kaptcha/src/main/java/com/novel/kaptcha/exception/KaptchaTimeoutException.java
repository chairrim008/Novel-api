package com.novel.kaptcha.exception;

/**
 * 验证码超时异常
 *
 * @author novel
 * @date 2019/9/28
 */
public class KaptchaTimeoutException extends KaptchaException {
    public KaptchaTimeoutException() {
        super("Kaptcha is timeout");
    }

    public KaptchaTimeoutException(String message) {
        super(message);
    }
}
