package com.novel.common.exception.business;

import com.novel.common.exception.base.BaseException;

/**
 * 全局业务异常
 *
 * @author novel
 * @date 2019/6/6
 */
public class BusinessException extends BaseException {
    public BusinessException(String s) {
        super(s);
    }
}
