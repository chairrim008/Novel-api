package com.novel.framework.config;

import com.novel.common.resource.ResourceType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 资源访问配置
 *
 * @author novel
 * @date 2019/6/4
 */
@ConfigurationProperties(prefix = ResourceConfig.RESOURCE_PREFIX)
@Data
public class ResourceConfig {
    public static final String RESOURCE_PREFIX = "resource";

    /**
     * 是否开启文件服务器存储文件
     */
    private boolean enable = false;
    /**
     * 是否开启缓存
     */
    private boolean cacheEnable = false;

    /**
     * 访问路径缓存时间
     */
    private int fileCacheMaxTime;

    /**
     * 图片服务器类型
     */
    private ResourceType fileType;
}
