package com.novel.framework.listener;

import cn.afterturn.easypoi.cache.manager.POICacheManager;
import com.novel.framework.utils.excel.FileLoaderImpl;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Excel 工具修改文件加载器的监听
 *
 * @author novel
 * @date 2020/3/3
 */
@Component
public class ExcelListener implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        POICacheManager.setFileLoader(new FileLoaderImpl());
    }
}
