package com.novel.system.service.impl;

import com.novel.common.constants.UserConstants;
import com.novel.common.utils.StringUtils;
import com.novel.system.domain.SysConfig;
import com.novel.system.mapper.SysConfigMapper;
import com.novel.system.service.SysConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 参数配置 服务层实现
 *
 * @author novel
 * @date 2020/07/08
 */
@Service
public class SysConfigServiceImpl implements SysConfigService {

    private final SysConfigMapper sysConfigMapper;

    public SysConfigServiceImpl(SysConfigMapper sysConfigMapper) {
        this.sysConfigMapper = sysConfigMapper;
    }

    /**
     * 查询参数配置信息
     *
     * @param id 参数配置ID
     * @return 参数配置信息
     */
    @Override
    public SysConfig selectConfigById(Long id) {
        return sysConfigMapper.selectConfigById(id);
    }

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    @Override
    public List<SysConfig> selectConfigList(SysConfig config) {
        return sysConfigMapper.selectConfigList(config);
    }

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public boolean insertConfig(SysConfig config) {
        return sysConfigMapper.insertConfig(config) > 0;
    }

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public boolean updateConfig(SysConfig config) {
        return sysConfigMapper.updateConfig(config) > 0;
    }

    /**
     * 保存参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public boolean saveConfig(SysConfig config) {
        Long id = config.getId();
        int rows = 0;
        if (StringUtils.isNotNull(id)) {
            rows = sysConfigMapper.updateConfig(config);
        } else {
            rows = sysConfigMapper.insertConfig(config);
        }
        return rows > 0;
    }

    /**
     * 删除参数配置信息
     *
     * @param id 参数配置ID
     * @return 结果
     */
    @Override
    public boolean deleteConfigById(Long id) {
        return sysConfigMapper.deleteConfigById(id) > 0;
    }

    /**
     * 批量删除参数配置对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public boolean batchDeleteConfig(Long[] ids) {
        return sysConfigMapper.batchDeleteConfig(ids) > 0;
    }

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public String checkConfigKeyUnique(SysConfig config) {
        Long configId = StringUtils.isNull(config.getId()) ? Long.valueOf(-1L) : config.getId();
        SysConfig info = sysConfigMapper.checkConfigKeyUnique(config.getConfigKey());
        if (StringUtils.isNotNull(info) && info.getId().longValue() != configId.longValue()) {
            return UserConstants.CONFIG_KEY_NOT_UNIQUE;
        }
        return UserConstants.CONFIG_KEY_UNIQUE;
    }

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数key
     * @return 参数键值
     */
    @Override
    public String selectConfigByKey(String configKey) {
        SysConfig config = new SysConfig();
        config.setConfigKey(configKey);
        List<SysConfig> sysConfigList = sysConfigMapper.selectConfigList(config);
        if (sysConfigList != null && sysConfigList.size() > 0) {
            return sysConfigList.get(0).getConfigValue();
        }
        return StringUtils.EMPTY;
    }

}
