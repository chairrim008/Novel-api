package com.novel.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author novel
 * @date 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysUser extends BaseModel {
    private static final long serialVersionUID = 1L;
    /**
     * 用户名
     */
    @NotBlank(message = "用户账号不能为空", groups = {AddGroup.class})
    @Size(max = 30, message = "用户账号长度不能超过30个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "登录名称")
    private String userName;

    /**
     * 部门ID
     */
    @Range(min = 0, message = "部门选择不正确", groups = {AddGroup.class, EditGroup.class})
    private Long deptId;

    /**
     * 密码
     */
//    @NotBlank(message = "用户密码不能为空", groups = {AddGroup.class})
    @Size(max = 16, message = "用户密码长度不能超过16个字符", groups = {AddGroup.class, EditGroup.class})
    @JsonIgnore
    private String password;

    /**
     * 用户姓名
     */
    @NotBlank(message = "用户姓名不能为空", groups = {AddGroup.class})
    @Size(max = 30, message = "用户姓名长度不能超过30个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "用户姓名")
    private String name;
    /**
     * 用户性别
     * 0=男,1=女,2=未知
     */
    @NotBlank(message = "用户性别不能为空", groups = {AddGroup.class})
    @Pattern(regexp = "^0|1|2$", message = "用户性别错误", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "用户性别", replace = {"男_0", "女_1", "未知_2"})
    private String sex;
    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    @Email(message = "邮箱格式不正确", groups = {AddGroup.class})
    @Size(max = 50, message = "邮箱长度不能超过50个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "邮箱", width = 15)
    private String email;

    /**
     * 手机号码
     */
//    @NotBlank(message = "用户手机不能为空", groups = {AddGroup.class})
    @Size(max = 11, message = "手机号码长度不能超过11个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "手机号码", width = 15)
    private String phoneNumber;

    /**
     * 用户年龄
     */
//    @Excel(name = "用户年龄")
    private Integer age;

    /**
     * 盐加密
     */
    @JsonIgnore
    private String salt;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Excel(name = "帐号状态", replace = {"正常_0", "停用_1"})
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;
    /**
     * 最后登陆IP
     */
    @Excel(name = "最后登陆IP", width = 15)
    private String loginIp;

    /**
     * 最后登陆时间
     */
    @Excel(name = "最后登陆时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;
    /**
     * 用户的角色Id
     */
    private List<Long> roleIds;

    /**
     * 用户的角色名
     */
    private String roleName;

    /**
     * 用户的角色对应的权限code
     */
    private List<String> permissionCodeList;

    /**
     * 部门对象
     */
    private SysDept dept;

    /**
     * 用户的岗位id
     */
    private List<Long> postIds;
    /**
     * 用户的岗位列表
     */
    private List<SysPost> posts;
    /**
     * 记住登录
     */
    @JsonIgnore
    private boolean rememberMe;

    public boolean isAdmin() {
        return isAdmin(this.getId());
    }

    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }
}